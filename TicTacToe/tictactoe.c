#include <stdio.h>
#include <string.h>
#include <time.h>
#include <conio.h>

char position[10] = { 'o', '1', '2', '3', '4', '5', '6', '7', '8', '9' };

char gameComplete();
void board(char *playername);



int main()
{
    int choice;
    int validity=0;
    char i;
    char *playername;
    int moves=0;
    printf("Kindly Enter your name: ");
    fgets(playername, 20, stdin);
    playername[strlen(playername)-1]='\0';
    printf("\nHello %s, Welcome to the Wonderful Tic-Tac-Toe\n", playername);

    do
    {
        board(playername);

        printf("%s, From thr grid, Enter the position for your X:  ", playername);
        scanf("%d", &choice);
        printf("\n");
        moves++;
        validity=0;
        
        //Placing X for the move

        if (choice == 1 && position[1] == '1')
            position[1] = 'X';
            
        else if (choice == 2 && position[2] == '2')
            position[2] = 'X';
            
        else if (choice == 3 && position[3] == '3')
            position[3] = 'X';
            
        else if (choice == 4 && position[4] == '4')
            position[4] = 'X';
            
        else if (choice == 5 && position[5] == '5')
            position[5] = 'X';
            
        else if (choice == 6 && position[6] == '6')
            position[6] = 'X';
            
        else if (choice == 7 && position[7] == '7')
            position[7] = 'X';
            
        else if (choice == 8 && position[8] == '8')
            position[8] = 'X';
            
        else if (choice == 9 && position[9] == '9')
            position[9] = 'X';
        
        // If the user enters an incorrect position, i.e, if the number is not within the range, or a place where there already X or O
        // Then the computer's chance must not occur and it should print invalid move and start again!   

        else
        {
            printf("Invalid move. Try again!\n ");
            validity=1;
            moves--;
        }

        i = gameComplete(); //Check if the player had already won,  return X if true!

        // Computer's Chance: Will only occur if the player has made a valid move;
        if (validity==0 && moves!=9 && i!='X'){
            int compMove=computerChance();
            printf("\nComputer places it's O at position %d\n\n",compMove);
            position[compMove]='O';
            moves++;
        }

        i=gameComplete(); //Check if the computer has won, return O if true

    }while (i ==  'o' && moves!=9);
    
    //Print the board with updated move
    board(playername);

    if (i == 'X'){
        printf("\aYay! %s wins!\n", playername);
        return 0;
    }

    else if (i == 'O'){
        printf("\aOh! Computer wins, Better luck next time, %s!\n", playername);
        return 0;
    }

    else
    {
        printf("\aGame draw");
        return 0;
    }
}


//Function for generating the value of valid postition for Computer to put its O

int computerChance(){
    srand(time(0));
    int randPosition;
    do
    {
        randPosition = (rand() % (9)) + 1;
    }
    while (position[randPosition] == 'X' || position[randPosition] == 'O');
    return randPosition;
}

//As soon as a three in a line occurs function returns char value of X or O at the position...
//If by the end of the game, if it is tied, function returns d...
//Iteratively it keeps returning o...
 

char gameComplete()
{
    if (position[1] == position[2] && position[2] == position[3])
        return position[1];
        
    else if (position[4] == position[5] && position[5] == position[6])
        return position[4];
        
    else if (position[7] == position[8] && position[8] == position[9])
        return position[7];
        
    else if (position[1] == position[4] && position[4] == position[7])
        return position[1];
        
    else if (position[2] == position[5] && position[5] == position[8])
        return position[2];
        
    else if (position[3] == position[6] && position[6] == position[9])
        return position[3];
        
    else if (position[1] == position[5] && position[5] == position[9])
        return position[1];
        
    else if (position[3] == position[5] && position[5] == position[7])
        return position[3];
        
    else if (position[1] != '1' && position[2] != '2' && position[3] != '3' &&
        position[4] != '4' && position[5] != '5' && position[6] != '6' && position[7] 
        != '7' && position[8] != '8' && position[9] != '9')
        return 'd';
    return 'o';
}

// Code for properly drawing Tic Tac Toe....                      

void board(char *playername)
{

    printf("%s (X)  -  Computer (O)\n\n\n", playername);


    printf("     |     |     \n");
    printf("  %c  |  %c  |  %c \n", position[1], position[2], position[3]);

    printf("_____|_____|_____\n");
    printf("     |     |     \n");

    printf("  %c  |  %c  |  %c \n", position[4], position[5], position[6]);

    printf("_____|_____|_____\n");
    printf("     |     |     \n");

    printf("  %c  |  %c  |  %c \n", position[7], position[8], position[9]);

    printf("     |     |     \n\n\n\n");
}
